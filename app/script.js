function changeBackgroundColor(sectionId) {
    const section = document.getElementById(sectionId);
    const originalColor = 'rgb(255, 255, 255)'; 
    const newColor = 'rgb(173, 216, 230)';       


    const currentColor = window.getComputedStyle(section).backgroundColor;

    if (currentColor === newColor) {
        section.style.backgroundColor = originalColor;
    } else {
        section.style.backgroundColor = newColor;
    }
}
