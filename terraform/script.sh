#!/bin/bash


sudo apt-get update
sudo apt-get upgrade -y

# Instalando docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh


curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner


# gitlab runner conseguir mexer no docker
sudo usermod -aG docker gitlab-runner

# registrando runner
sudo gitlab-runner register \
    --non-interactive \
    --executor "shell" \
    --url "https://gitlab.com/" \
    --token "<token-do-site>" \





