# Projeto Simples Usando Terraform, Docker e Pipeline Gitlab

- Gabriel Pinheiro Palitot Pereira

# Inicializando o servidor

Na pasta terraform, vamos iniciar com `terraform init` e depois `terraform plan`

<img src="./img/terraformPlan.png">

No script.sh está o seguinte conteúdo de script:

```bash
#!/bin/bash


sudo apt-get update
sudo apt-get upgrade -y

# Instalando docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh


curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner


# gitlab runner conseguir mexer no docker
sudo usermod -aG docker gitlab-runner

# registrando runner
sudo gitlab-runner register \
    --non-interactive \
    --executor "shell" \
    --url "https://gitlab.com/" \
    --token "<token do site do gitlab>" \
```

Onde basicamente é instalado o docker na máquina virtual, o gitlab runner e é registrado o  runner.

Lembrando de criar o repositório no gitlab e verificar se o runner foi registrado corretamente:

<img src="./img/runner.png">

Como tudo está certo, então vamos finalmente rodar `terraform apply`

<img src="./img/servidor.png">

Como o servidor subiu e aparentemente está tudo correto, vamos para o próximo passo que é a criação da aplicação e docker.

Obs: Depois que rodei o terraform apply eu modifiquei o valor "token" no script para que o token não vá para um repositório publico.

# Criar um dockerfile com as definições de um servidor apache (httpd)

Primeiramente, vamos criar um dockerfile usando como base a imagem do servidor apache disponível no dockerhub. O arquivo Dockerfile está dentro da pasta app.

```dockerfile
FROM httpd:latest

WORKDIR /usr/local/apache2/htdocs

COPY . /usr/local/apache2/htdocs/

EXPOSE 80
```

A aplicação é bem simples. É uma página em html e estilizada com css que possui campos "home", "estudos" e "contato" e na seção "home" possui um botão para mudar a cor de fundo da seção.

# Criar um pipeline no GitLab de criação da imagem a partir do dockerfile.

O pipeline se encotra em ".gitlab-ci.yml". E o seu conteúdo é esse:

```yml
stages:
  - build
  - deploy

variables:
  VERSION: "1.0"
  IMAGE: "avanti-challenge"
  DOCKERHUB_USERNAME: "gabrielpalitot"
  PATH_APP: "app/."

create_images:
  stage: build
  tags:
    - aws 
  before_script:
    - docker login -u $user -p $password
  script:
    - docker build -t $DOCKERHUB_USERNAME/$IMAGE:$VERSION $PATH_APP
    - docker push $DOCKERHUB_USERNAME/$IMAGE:$VERSION

run_docker:
  stage: deploy
  tags:
    - aws
  before_script:
    - if [ $(docker ps -aq | wc -l) -gt 0 ]; then docker rm $(docker ps -aq) --force; fi
    - if [ $(docker images -q | wc -l) -gt 0 ]; then docker rmi $(docker images -q); fi
  script:
    - docker pull $DOCKERHUB_USERNAME/$IMAGE:$VERSION
    - docker run -dti -p 80:80 --name app-docker $DOCKERHUB_USERNAME/$IMAGE:$VERSION
```


Vemos que é construído a imagem e depois enviado para o docker hub. Usei variáveis para facilitar caso haja alguma mudança. Por fim, é feito o pull e o run no servidor. Além disso, personalizei o script para verificar se há algum conteiner ou imagem antes de apagar para que não dê erro no pipeline.

Lembrando antes de criar as variáveis "user" e "password" dentro do repositório do gitlab. São as credenciais do meu dockerhub.

<img src="./img/userEpassword.png">

Agora, vamos realizar o push dos arquivos no repositório e ver se finalmente deu certo.

Vamos ver o resultado do pipeline:

<img src="./img/sucessPipeline.png">

Por fim, falta apenas verificarmos se o servidor está com a nossa aplicação funcionando: 

<img src="./img/funcionando.png">

Vamos testar a mudança de cor ao apertar o botão:

<img src="./img/BotaoFuncionando.png">

Por fim, aqui está o docker hub com a imagem e sua tag:

<img src="./img/dockerhubimage.png">



